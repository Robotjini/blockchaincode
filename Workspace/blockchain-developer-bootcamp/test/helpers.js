export const ETH_ADDRESS = '0x0000000000000000000000000000000000000000';

export const EVM_REVERT = 'VM Exception while processing transaction: revert';

export function tokens(n) {
  return web3.utils.toWei(n, 'ether');
}

