pragma solidity ^0.5.0;

contract Decentragram {
  // Code goes here...
  string public name = "Decentragram";

  //Store Images
  struct Image {
    uint id;
    string hash;
    string description;
    uint tipAmount;
    address payable author;
  }

  uint public
  mapping(uint => Image) public images;

  //Create Images
  function uploadImage() public {
    images[1] = Image(1, 'abc123', 'Hello World', 0, address(0x0));
  } 

  //Tip Images

}